<?php


namespace App\Services\Formatters\Interfaces;


interface FormatterInterface
{
    public static function format(array $data): string;
}