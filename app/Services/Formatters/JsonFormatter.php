<?php


namespace App\Services\Formatters;


use App\Services\Formatters\Interfaces\FormatterInterface;

class JsonFormatter implements FormatterInterface
{

    /**
     * @param array $data
     * @return string
     */
    public static function format(array $data): string
    {
        return json_encode($data, JSON_THROW_ON_ERROR);
    }

}