<?php


namespace App\Services\Formatters;


use App\Services\Formatters\Interfaces\FormatterInterface;

class HTMLFormatter implements FormatterInterface
{

    /**
     * @param array $data
     * @return string
     */
    public static function format(array $data): string
    {
        $result = '<table class="table">';

        foreach ($data as $row) {
            $result .= '<tr>';
            foreach ($row as $key => $value) {
                $result .= "<td data-column='" . $key . "'>$value</td>";
            }
            $result .= '</tr>';
        }

        $result .= '</table>';

        return $result;
    }
}