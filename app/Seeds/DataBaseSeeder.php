<?php


namespace App\Seeds;

use App\Models\PostTag;
use App\Models\Tag;
use App\Models\User;
use Faker\Factory;

/**
 * Class DataBaseSeeder
 * @package App\Seeds
 */
class DataBaseSeeder
{
    /**
     * @return void
     */
    public static function seeder()
    {

        $faker = Factory::create();

            //Создание тегов

        for ($i = 0; $i < 10; $i++) {

            $tag = new Tag();
            $tag->name = $faker->colorName;
            $tag->save();
        }
            //Создание пользователей

            for ($u = 0; $u < 3; $u++) {

                $user = new User();
                $user->email = $faker->email;
                $user->login = $faker->userName;
                $user->password = password_hash("$faker->password", PASSWORD_ARGON2I);
                $user->save();
                $user->refresh();
                $user->userInfo()->create(
                    [
                        'user' => "$user->id",
                        'nickname' => "$faker->firstName",
                        'photo' => "photo of $user->login",
                    ]
                );
                $user->push();
                $user->refresh();

                // Создание постов

                for ($p = 0; $p < 5; $p++) {
                    $user->posts()->create([
                        'title' => "title $p",
                        'description' => $faker->realText($maxNbChars = 50, $indexSize = 2),
                        'author' => $user->id,
                        'createdAt' => date('Y-m-d h:m:s'),
                        'updatedAt' => date('Y-m-d h:m:s'),
                    ]);
                }

                //Присвоение тегов

                    $postsIds = $user->posts->pluck('id')->toArray();
                    if ($u == 1) {
                        $tagId = 1;
                        $postsAndTags = [];
                        foreach ($postsIds as $key => $post) {
                            $tagId = ($key % 15 === 0) ? (++$tagId) : $tagId;
                            $postsAndTags[] = [
                                'post_id' => $post,
                                'tag_id' => $tagId,
                            ];
                        }
                        PostTag::insert($postsAndTags);
                        } else {
                            foreach ($user->posts as $post) {
                                for ($t = 0; $t < 3; $t++) {
                                    $tagIdRandom = rand(1, 10);
                                    $post->tags()->attach($tagIdRandom);
                    }
                }
            }
        }
    }
}