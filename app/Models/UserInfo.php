<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class UserInfo extends Model
{

    protected $table = "user_info";

    /**
     * The name of the "createdAt" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'createdAt';
    /**
     * The name of the "updatedAt" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updatedAt';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'nickname', 'photo'];


    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user');
    }
}