<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Post
 * @package App\Models
 */
class Post extends Model
{
    use SoftDeletes;

    /**
     * The name of the "createdAt" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'createdAt';
    /**
     * The name of the "updatedAt" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updatedAt';
    /**
     * The name of the "deletedAt" column.
     *
     * @var string|null
     */
    const DELETED_AT = 'deletedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'author'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'author');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id');
    }

}