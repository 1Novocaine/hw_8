<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class Tag extends Model
{

    /**
     * The name of the "createdAt" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'createdAt';
    /**
     * The name of the "updatedAt" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updatedAt';
    /**
     * The name of the "deletedAt" column.
     *
     * @var string|null
     */
    const DELETED_AT = 'deletedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


}