<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class User
 * @package App\Models
 */
class User extends Model
{
    use SoftDeletes;

    /**
     * The name of the "createdAt" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'createdAt';
    /**
     * The name of the "updatedAt" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updatedAt';
    /**
     * The name of the "deletedAt" column.
     *
     * @var string|null
     */
    const DELETED_AT = 'deletedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'login', 'password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userInfo()
    {
        return $this->hasOne(UserInfo::class, 'user', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'author', 'id');
    }
}