<?php


namespace App\Models;

use App\DB\DBConnector;
use Exception;
use PDO;

/**
 * Class Model
 *
 * ::find()
 *
 * ->delete()
 * ->save()
 */

abstract class Model
{
    /**
     * @param int $id
     * @return Model
     * @throws Exception
     */
    public static function find(int $id)
    {
        $table = self::getTable(static::class);
        $class = static::class;
        $columns = implode(',', $class::$props);
        $sql = "
                select $columns from $table 
                where id = :id
                ";
        $pdo = DBConnector::getInstance();
        $query = $pdo->prepare($sql);
        $query->execute([':id' => $id]);

        return $query->fetchObject(static::class);
    }


    /**
     * @return int
     * @throws \Exception
     */
    public function delete(): int
    {
        $table = self::getTable(static::class);
        $sql = "
                delete * from $table 
                where id = :id
                ";
        $pdo = DBConnector::getInstance();
        $query = $pdo->prepare($sql);
        $check = $query->execute([':id' => $this->id]);
        if(!$check)
        {
            throw new \Exception('User not found.');
        }

        return $query->rowCount();
    }

    /**
     * @return int
     * @throws Exception
     */
    public function save(): int
    {
        $table = self::getTable(static::class);
        if ($this->id)
        {
            $sql = "
                update $table
                set 
                    email = :email,
                    login = :login 
                where id = :id
                ";
            $pdo = DBConnector::getInstance();
            $query = $pdo->prepare($sql);
            $check = $query->execute([
                ':id' => $this->id,
                ':email' => $this->email,
                ':login' => $this->login
            ]);

            if(!$check)
            {
                throw new \Exception('User not found.');
            }

            return $query->rowCount();
        }
        $sql = "
                    insert into $table
                    (email, login, password)
                    values 
                    (:email, :login, :password)
                    ";
        $pdo = DBConnector::getInstance();
        $query = $pdo->prepare($sql);

        $check = $query->execute([
            ':email' => $this->email,
            ':login' => $this->login,
            ':password' => $this->password
        ]);
        if(!$check)
        {
            throw new \Exception('1');
        }

        return $query->rowCount();
    }

    /**
     * @param string $class
     * @return string
     */
    private static function getTable (string $class)
    {
        $parts = explode('\\', $class);
        $table = $parts[count($parts)-1];

        return mb_strtolower($table) . 's';
    }
}