<?php

use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use App\Models\User;
use App\Models\UserInfo;
use App\Seeds\DataBaseSeeder;


require_once '../vendor/autoload.php';
require_once '../app/config/db.php';

if (isset($_GET['seed'])) {
    try {
        DatabaseSeeder::seeder();

        echo '<a href="index.php">Назад</a>';

    } catch (Exception $e) {
        echo "<p>Fail</p>";
        echo '<p>' . $e->getMessage() . "</p>";
        echo '<a href="index.php" style="display: block">Назад</a>';

    }
}
if (isset($_GET['truncate-tables'])) {
    try {
        User::truncate();
        UserInfo::truncate();
        Tag::truncate();
        Post::truncate();
        PostTag::truncate();

        header("Location: index.php");

    } catch (Exception $e) {
        echo "<p>Fail</p>";
        echo '<p>' . $e->getMessage() . "</p>";
        echo '<a href="index.php" style="display: block">Назад</a>';
    }
    exit(0);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ДЗ 8. Работа с Eloquent моделями</title>
        <link rel="stylesheet" href="">
    </head>
        <body>
            <?php
            echo '<a href="index.php?seed" style="display: block">Обновить данные в базе данных</a>';
            echo '<a href="index.php?truncate-tables" style="display: block">Очистить базу данных</a>';
            ?>
        </body>
</html>